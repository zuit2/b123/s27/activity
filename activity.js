let http = require('http');

let users = [

	{
		username: "jeff_chan17",
		password: "balliflife17"
	},{
		username: "d_miranda0",
		password: "mirandaPG"
	},{
		username: "arwind_spiderMan",
		password: "imspiderMan"
	},

]

http.createServer(function(req,res){

	if(req.url === "/users" && req.method === "GET"){
		res.writeHead(200, {'Content-Type':'application/json'});
		res.end(JSON.stringify(users));

	}
	else if(req.url === "/users" && req.method === "POST"){
		
		let requestBody = "";

		req.on('data',function(data){
			requestBody+=data;
		})

		req.on('end',function(){
			requestBody = JSON.parse(requestBody);

			let newUser = {
				username: requestBody.username,
				password: requestBody.password
			}

			users.push(newUser);
			console.log(users);

			res.writeHead(200,{'Content-Type':'application/json'});
			// res.end(JSON.stringify(newUser));
			res.end('Registration Successful')

		})

	}
	else if(req.url === "/users/login" && req.method === "POST"){
		
		let requestBody = "";

		req.on('data',function(data){
			requestBody+=data;
		})

		req.on('end',function(){
			requestBody = JSON.parse(requestBody);

			let foundUser = users.find((user)=>{
			return user.username === requestBody.username && user.password === requestBody.password;
			})

			// console.log(foundUser);

			res.writeHead(200,{'Content-Type':'application/json'});
			foundUser ? res.end(JSON.stringify(foundUser)) : res.end('Login Failed. Wrong Credentials.');
				

		})
		
	}
	else {

		res.writeHead(404,{'Content-Type':'text/plain'});
		res.end('Resource Not Found');
	}

}).listen(8000);


// console.log(`Server is running at port 8000`);